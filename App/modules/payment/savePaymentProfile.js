'use strict';

const HttpStatus = require('http-status-codes');
const failure = require('../../helper/helper').failure;
const success = require('../../helper/helper').success;

const savePayment = async(req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await savePaymentProfile(req);
        res.status(HttpStatus.OK).send(success("Payment Profile Added", resultObj));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}


const validate = async(req) => {
    if (!req.body.flexresponse) throw new Error("Flexresponse is required");
    // if (!req.body.token_expiry) throw new Error("Token Expiry is required");
    // if (!req.body.profile_card_number) throw new Error("Profile Card Number is required");
}


const savePaymentProfile = async(req) => {

    let query = 'INSERT INTO `payment_profile`(`token`, `user_id`) VALUES (?,?)';

    return await req.db(query, [req.body.flexresponse, req.authUser.id]);

}

module.exports = {
    savePayment: savePayment
};