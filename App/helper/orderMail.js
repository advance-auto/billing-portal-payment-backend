const axios = require('axios')

const sendMail = async (body) => {

    axios.post('https://dev-market.liiighthouse.net/index.php/api/userApi/order_mail', body)
        .then(res => {
            console.log(`statusCode: ${res.status}`);
        })
        .catch(error => {
            console.error(error)
        })

};

module.exports = { sendMail };