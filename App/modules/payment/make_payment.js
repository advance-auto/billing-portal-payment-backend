'use strict';

const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
let cybersourceRestApi = require('cybersource-rest-client');
const { formatDate, success, failure, valueCheck } = require('../../helper/helper');
const sendMail = require('../../helper/orderMail').sendMail;

const getReceipt = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await generateReceipt(req);
        let receiptData = await savePaymentHistory(resultObj, req);
        // receiptData.order = await updateOrderDetails(req)
        res.status(HttpStatus.OK).send(success("Receipt", receiptData));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}



const validate = async (req) => {
    if (!req.body.flexresponse && (!req.body.customer && !req.body.code)) throw new Error("FLEXRESPONSE or (Customer ID and Code) is required");
    if (!req.body.shipping_address) throw new Error("SHIPPING ADDRESS is required");
    if (!req.body.billing_address) throw new Error("BILLING ADDRESS is required");
    if (!req.body.amount) throw new Error("AMOUNT is required");
    if (!req.body.unique_id) throw new Error("Unique ID is required");

    let data = (await req.db(`SELECT id, user_id, shipping_email FROM orders WHERE unique_id = ?`, [req.body.unique_id]))[0];
    if (data.length == 0) throw new Error('Invalid UNIQUE ID');

    let { id, user_id, shipping_email } = data;

    if (!id) throw new Error("Invalid Unique ID");
    req.body.user_type = !user_id ? "Guest" : "Registered";
    req.body.order_id = id;
    req.body.user_id = user_id;
    req.body.shipping_email = shipping_email;

}

const generateReceipt = async (req) => {

    let tokenResponse = req.body.flexresponse

    let payment = req.app.get('config').payment;

    let reqBody = req.body;

    const configObj = payment.configObj;

    let instance = new cybersourceRestApi.PaymentsApi(configObj);

    let clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsClientReferenceInformation();

    clientReferenceInformation.code = req.body.shipping_email; // reqBody.clientReferenceInformationcode; // clientReferenceInformation.code should be parsed from the token barer from the request object 

    var processingInformation = new cybersourceRestApi.Ptsv2paymentsProcessingInformation();
    processingInformation.commerceIndicator = "internet"; //reqBody.commerceIndicator;

    var amountDetails = new cybersourceRestApi.Ptsv2paymentsOrderInformationAmountDetails();
    amountDetails.totalAmount = reqBody.amount;
    amountDetails.currency = req.app.get('config').currency;

    let query = 'SELECT country, first_name,last_name,phone,address1,postcode,city as locality,state as administrativeArea,email,address2,state as district FROM `addresses` WHERE id = ?';
    let result = await req.db(query, [req.body.shipping_address])
    if (!result) throw new Error('Invalid Address ID');
    result = result[0]

    let billTo = new cybersourceRestApi.Ptsv2paymentsOrderInformationBillTo();
    billTo.country = valueCheck(result.country, "Country cannot be empty!");
    billTo.firstName = valueCheck(result.first_name, "first_name cannot be empty!");
    billTo.lastName = valueCheck(result.last_name, "last_name cannot be empty!") //? result.last_name : "Las";
    billTo.phoneNumber = valueCheck(result.phone, "phone cannot be empty!");
    billTo.address1 = valueCheck(result.address1, "address1 cannot be empty!");
    billTo.postalCode = valueCheck(result.postcode, "postcode cannot be empty!");
    billTo.locality = valueCheck(result.locality, "locality cannot be empty!"); // ?
    billTo.administrativeArea = valueCheck(result.administrativeArea, "administrativeArea cannot be empty!"); // ?
    billTo.email = valueCheck(result.email, "email cannot be empty!");
    billTo.address2 = valueCheck(result.address2, "address2 cannot be empty!");
    // billTo.district = result.district; // ?
    // billTo.buildingNumber = result.buildingNumber; // ?

    let orderInformation = new cybersourceRestApi.Ptsv2paymentsOrderInformation();
    orderInformation.amountDetails = amountDetails;
    orderInformation.billTo = billTo;

    if (req.body.save && req.body.save === 'true') {
        processingInformation.actionList = ["TOKEN_CREATE"];
        processingInformation.actionTokenTypes = ["customer", "paymentInstrument", "shippingAddress"];
    }

    var tokenInformation = new cybersourceRestApi.Ptsv2paymentsTokenInformation();
    tokenInformation.transientTokenJwt = tokenResponse ? tokenResponse : req.body.code;

    let paymentInformation = new cybersourceRestApi.Ptsv2paymentsPaymentInformation();

    if (req.body.customer && req.body.code) {

        let code = req.body.code;
        let profile = await req.db('SELECT `id`, `card_name`, `instrument_identifier`, `payment_instrument` FROM `cards` WHERE user_id = ? AND id = ?', [req.body.user_id, req.body.customer]);
        if (!profile.length) throw new Error("Customer ID is not found for this user.")
        let card_type = profile[0].card_name;

        paymentInformation.instrumentIdentifier = { id: profile[0].instrument_identifier };
        paymentInformation.card = await getPaymentInstrument(cybersourceRestApi, profile[0].payment_instrument, card_type, req);
    }


    var request = new cybersourceRestApi.CreatePaymentRequest();

    request.clientReferenceInformation = clientReferenceInformation;
    request.processingInformation = processingInformation;
    request.orderInformation = orderInformation;
    request.tokenInformation = tokenInformation;
    if (req.body.customer) request.paymentInformation = paymentInformation;
    // if (req.body.customer) request.paymentInformation = paymentInformation;

    console.log(request);


    let resPro = new Promise((err, resolve) => {
        instance.createPayment(request, function (error, data, response) {

            if (error) err(error);
            else if (data) resolve(data);
        });
    })

    return await resPro.then(
        err => { return { error: true, data: err } },
        responseData => { return { error: false, data: responseData } }
    );
}

const savePaymentHistory = async (response, req) => {

    if (response.error)
        throw new Error(response.data.response.text);

    // Save a payment profile
    if ((req.body.save && req.body.save === 'true') && response.data.tokenInformation) {

        var decoded = await jwt.decode(req.body.flexresponse);
        const profile_card_number = (decoded && decoded.data && decoded.data.number) ? decoded.data.number : false;
        if (!profile_card_number) throw new Error('invalid token');

        let profile = await req.db('SELECT * FROM `cards` WHERE user_id = ?', [req.body.user_id]);
        const firstTime = profile[0] ? false : true;
        const cardName = response.data.paymentInformation.card.type; //001
        const lastFourDigit = profile_card_number.slice(profile_card_number.length - 4);
        const date = formatDate(1);
        const { paymentInstrument: { id: paymentInstrument }, shippingAddress: { id: shippingAddress }, instrumentIdentifier: { id: instrumentIdentifier } } = response.data.tokenInformation;

        profile = JSON.parse(JSON.stringify(profile));

        let isCardAdded = profile.reduce((isAlreadyAdded, card) => {
            if (card.instrument_identifier === instrumentIdentifier) return true;
            if (isAlreadyAdded) return true;
        }, false);

        if (!isCardAdded) {

            let query = 'INSERT INTO `cards`(`user_id`, `card_name`, `instrument_identifier`, `payment_instrument`, `shipping_address`, `is_default`, `created_at`) VALUES (?,?,?,?,?,?,?);';
            let values = [req.body.user_id, cardName, instrumentIdentifier, paymentInstrument, shippingAddress, firstTime, date];

            req.db(query, values);
        }
    }

    let status;
    if (response.data.payment_info && response.data.payment_info.status) status = response.data.payment_info.status;
    else if (response.data.status) status = response.data.status;
    else status = "unknown";

    let responceData = JSON.stringify(response.data)
    let query = 'INSERT INTO `payment`(`status`, `response`, `order_id`, `totalAmount`, `currency`, `payment_id`, `cybersource_status`) VALUES (?,?,?,?,?,?,?)';
    let result = await req.db(query, [((status == "AUTHORIZED") ? "successful" : "failure"), responceData, req.body.order_id, req.body.amount, req.app.get('config').currency, (response.data.id ? response.data.id : null), status]);

    //Update Order Details (order_id = req.body.order_id)
    /* Changing the status of the order from PENDING to PLACED => For ecommerce order products  (This is applicable for only product purchase orders.
         The parameters needed to update the oredr status will be sent from the frontend
         order_id => integer
    */
    //    Get Placed and Declined id

    // IF payment successful
    // Update Orders set paid = 1, {1:true, 0:false};
    // Update order_products set b.buyer_status_id = 2, b.seller_status_id = 2, b.buyer_status = PLACED, b.seller_status = PLACED;
    if (status == "AUTHORIZED")
        query = `UPDATE orders as a, order_products as b SET a.paid = 1, b.buyer_status_id = 2, b.seller_status_id = 2, b.buyer_status = "PLACED", b.seller_status = "PLACED" WHERE a.id = b.order_id AND a.id = ?`;


    // Else payment usuccessful
    // Update Orders set paid = 0, {1:true, 0:false};
    // Update order_products set buyer_status_id = 16, seller_status_id = 16, buyer_status = DECLINED, seller_status = DECLINED;
    else
        query = `UPDATE orders as a, order_products as b SET a.paid = 0, b.buyer_status_id = 16, b.seller_status_id = 16, b.buyer_status = "PENDING", b.seller_status = "PENDING" WHERE a.id = b.order_id AND a.id = ?`;

    await req.db(query, [req.body.order_id]);


    let slug = ((await req.db(`SELECT slug FROM order_products WHERE order_id = ?`, [req.body.order_id]))[0]).slug;

    req.db(`UPDATE order_status_history SET datetime = ?, status = 1 WHERE order_product_slug = ? AND order_status = 'PLACED'`, [new Date(), slug])

    // Append Order_id into the response.
    response.data.order_id = req.body.order_id;

    // Send Order Confirmation Email
    sendMail({
        order_id: req.body.order_id,
        api_key: req.app.get('config').api_key
    });

    return response.data;
}

const getPaymentInstrument = async (cybersourceRestApi, token, card_type, req) => {

    let config = req.app.get('config');
    let secret = config.secret_key;
    let apiClient = new cybersourceRestApi.ApiClient();
    let instance = new cybersourceRestApi.PaymentInstrumentApi(config.payment.configObj, apiClient);

    let resp = await new Promise((error, data) => {
        instance.getPaymentInstrument(token, [], function (err, dat, res) {
            if (err) error(err);
            if (dat) data(JSON.stringify(dat.card));
        })
    }).then(error => 'error: ' + error, data => data);

    let card = resp ? JSON.parse(resp) : {};
    card.type = card_type;

    // card.securityCode = req.body.code;

    // await jwt.verify(req.body.code, async function (err, code) {
    //     if (err) throw new Error(err);
    //     return code;
    // });

    return card;
}

const updateOrderDetails = async (req, response) => {


    let order_id = req.body.order_id;
    let { transactionID, status } = getTransactionId(response);

    // update orders -> order_transaction_id with transaction id
    let query = `UPDATE orders SET order_transaction_id = ? WHERE id = ?`;
    await req.db(query, [transactionID, order_id]);

    // get id from order_status
    let order_status_id = (await req.db(`SELECT id from order_statuses WHERE name = ?`, [status == "PLACED" ? status : "CANCELLED"]))[0].id;

    // update order_products
    query = `UPDATE order_products SET 
    buyer_status_id = ?,
    seller_status_id = ?,
    buyer_status = ?,
    seller_status = ? 
    WHERE id = ?`;

    await req.db(query, [order_status_id, order_status_id, status, status, order_id]);

    // return updated
    return (await req.db(`SELECT * from orders WHERE id = ?`, [order_id]))[0];
}

function getTransactionId(response) {
    return { transactionID: response.id, status: response.processorInformation.responseCode === "100" ? "PLACED" : "CANCELLED" };
}

module.exports = {
    getReceipt
};