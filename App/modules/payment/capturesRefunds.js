'use strict';

const HttpStatus = require('http-status-codes');
let cybersourceRestApi = require('cybersource-rest-client');
const { formatDate, success, failure, valueCheck } = require('../../helper/helper');

const capturesRefunds = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await reversal(req);
        let status = await updateStatus(resultObj, req);
        res.status(HttpStatus.OK).send(success("data", status));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}



const validate = async (req) => {
    if (!req.body.payment_id) throw new Error("Payment ID is Required!");
    if (!req.body.unique_id) throw new Error("Unique ID is Required!");
    let data = JSON.parse(JSON.stringify(await req.db(`SELECT p.currency, p.order_id, o.user_email FROM payment as p INNER JOIN orders as o ON p.order_id = o.id WHERE payment_id = ?`, [req.body.payment_id])))[0];
    let data2 = JSON.parse(JSON.stringify(await req.db(`SELECT return_amount, reason FROM return_product WHERE unique_id = ?`, [req.body.unique_id])))[0];
    if (!data.order_id) throw new Error("Invalid Payment ID");
    req.body.order_id = data.order_id;
    req.body.totalAmount = data2.return_amount;
    req.body.user_email = data.user_email;
    req.body.currency = data.currency;
    req.body.reason = data2.reason;

}

const reversal = async (req) => {

    let payment = req.app.get('config').payment;

    const configObj = payment.configObj;


    var apiClient = new cybersourceRestApi.ApiClient();
    var requestObj = new cybersourceRestApi.AuthReversalRequest();

    var clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsidreversalsClientReferenceInformation();
    clientReferenceInformation.code = req.body.user_email;
    requestObj.clientReferenceInformation = clientReferenceInformation;

    var orderInformation = new cybersourceRestApi.Ptsv2paymentsidrefundsOrderInformation();
    var orderInformationAmountDetails = new cybersourceRestApi.Ptsv2paymentsidcapturesOrderInformationAmountDetails();
    orderInformationAmountDetails.totalAmount = req.body.totalAmount;
    orderInformationAmountDetails.currency = req.body.currency;
    orderInformation.amountDetails = orderInformationAmountDetails;

    requestObj.orderInformation = orderInformation;


    var instance = new cybersourceRestApi.RefundApi(configObj, apiClient);

    return await new Promise((err, resolve) => {
        instance.refundCapture(requestObj, req.body.payment_id, function (error, data, response) {
            if (error) err(error)
            else if (data) resolve(data);
        });
    }).then(
        err => { return { error: true, data: err } },
        responseData => { return { error: false, data: responseData } }
    );

}

const updateStatus = async ({ data, error }, req) => {

    let response = JSON.stringify(data);
    let payment_id = req.body.payment_id;
    let currency = req.body.currency;
    let order_id = req.body.order_id;
    let order_unique_id = req.body.unique_id;
    let status = data.status;
    let amount = req.body.totalAmount;
    let refund_id = data.id ? data.id : '0';

    req.db(`INSERT INTO payment_refunds (response, payment_id, currency, order_id, order_unique_id, status, amount, refund_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`, [response, payment_id, currency, order_id, order_unique_id, status, amount, refund_id]);

    // if (error) throw new Error(data.response.text);

    let resp = { status, amount, refund_id };

    return resp;
}

module.exports = {
    capturesRefunds
};
