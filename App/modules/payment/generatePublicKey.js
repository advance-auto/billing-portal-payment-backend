'use strict';

const HttpStatus = require('http-status-codes');
const failure = require('../../helper/helper').failure;
const success = require('../../helper/helper').success;

let cybersourceRestApi = require('cybersource-rest-client');

const generatePublicKey = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let key = await generateKey(req);
        res.status(HttpStatus.OK).send(success("Public key generated!", key));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error))
    }
}

const validate = async (req) => {
}

const generateKey = async (req) => {

    let configObject = req.app.get('config').payment.configObj;

    let apiClient = new cybersourceRestApi.ApiClient();
    let requestObj = new cybersourceRestApi.GeneratePublicKeyRequest();
    requestObj.encryptionType = 'RsaOaep256';
    requestObj.targetOrigin = req.app.get('config').payment.front_end_url;
    let format = "JWT";
    let _key;

    let promise = new Promise(function (reject, resolve) {
        var instance = new cybersourceRestApi.KeyGenerationApi(configObject, apiClient);
        instance.generatePublicKey(format, requestObj, function (error, data, response) {
            if (error) {
                console.log(error)
                reject("Fatal Payment system Error!");
            }
            else if (data) {
                resolve(data.keyId);
            }
        });
    })

    return await promise.then((error) => { throw Error(error) }, (value) => value);
}

module.exports = {
    generatePublicKey: generatePublicKey
};
