var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();

let db = require('./App/core/db');
var indexRouter = require('./routes/index');
var paymentRouter = require('./routes/payment');
var cors = require('cors')

let config = require('./App/config')
config = new config();

var app = express();
app.use(cors());

// for parsing application/json
app.use(bodyParser.json());

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));


app.set('config', config);
app.use(db.db());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

indexRouter(app);
paymentRouter(app);

console.log("Running on port =>", app.get('config').port);
module.exports = app;
