let test = require('../App/modules/test');
let Security = require('../App/core/authentication');
let ENV = require('../App/core/setEnv');

module.exports = (app) => {
  app.get("/", (req, res, next) => {
    res.send('Payment API');
  });
  app.get("/test", ENV.setEnv, test.test);
};