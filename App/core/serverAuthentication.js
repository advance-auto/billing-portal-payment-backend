var md5 = require('md5');

const EAUTH = async (req, res, next) => {

    // Get api_key FROM HEADER
    let API_KEY = req.headers.authorization;

    // IF API-KEY NOT PRESENT
    // ERROR
    if (!API_KEY) {
        res.status(401).json({ status: false, message: "UNAUTHORIZED!" });
    }

    // ENCRYPT THE API-KEY
    let ENCRYPTED = md5(API_KEY);

    // ENCRYPTED NOT IN system_preferences=>value:ENCRYPTED
    // ERROR
    let query = `SELECT \`key\` from system_preferences WHERE value = "${ENCRYPTED}"`
    let data = (JSON.parse(JSON.stringify(await req.db(query))))[0];
    const MARKET_ORGIN = (JSON.parse(JSON.stringify(await req.db('SELECT value from system_preferences WHERE `key` = \'market_url\''))))[0].value;

    if (!data) {
        res.status(401).json({ status: false, message: "UNAUTHORIZED!" });
    }

    // PROCEED
    else if (data.key == "payment_module_api_key" && req.get('origin') == MARKET_ORGIN)
        next();
    else
        res.status(401).json({ status: false, message: "UNAUTHORIZED!" });

};

module.exports = { EAUTH };