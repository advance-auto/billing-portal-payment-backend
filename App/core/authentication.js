'use strict';

const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const failure = require('../helper/helper').failure;
const Constants = require('../helper/Constants');

const authorize = (req, res, next) => {

    let serviceConfig = req.app.get('config');
    let bearerToken = req.headers.authorization;

    if (!bearerToken) {
        return res.status(HttpStatus.FORBIDDEN).json(failure(
            Constants.FORBIDDEN_ERROR, HttpStatus.FORBIDDEN
        ));
    }

    var bearer = bearerToken.split(' ')[0].toLowerCase();
    var token = bearerToken.split(' ')[1];

    if (bearer != 'bearer' || !token) {
        return res.status(HttpStatus.FORBIDDEN).json(failure(
            Constants.FORBIDDEN_ERROR, HttpStatus.FORBIDDEN
        ));
    }

    // verifies secret and checks user
    jwt.verify(token, serviceConfig.secret_key, async function (err, user) {

        if (err) {

            let errorMessage = Constants.INVALID_TOKEN;

            if (err.name == "TokenExpiredError") {
                errorMessage = Constants.EXPIRED_TOKEN;
            }

            return res.status(HttpStatus.UNAUTHORIZED).json(failure(errorMessage, HttpStatus.UNAUTHORIZED));

        } else {

            req.authUser = {
                id: user.user_id,
                email: user.email
            }

            delete user.iat;
            delete user.exp;
            delete user.nbf;
            delete user.jti; //We are generating a new token

            next();
        }
    });
}

module.exports = {
    authorize: authorize
};