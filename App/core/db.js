const mysql = require('mysql2/promise');
let config = require('../config/');
config = new config();

function db() {
  return function (req, res, next) {
    req.db = async (sql, params) => {
      const connection = await mysql.createConnection(config.db);
      const [results,] = await connection.execute(sql, params);
      connection.end();
      return results;
    }
    next();
  }
}

// function db() {
//   return function (req, res, next) {
//     req.db_dev = async (sql, params) => {
//       const connection = await mysql.createConnection(config.db_dev);
//       const [results,] = await connection.execute(sql, params);
//       connection.end();
//       return results;
//     }
//     req.db_stage = async (sql, params) => {
//       const connection = await mysql.createConnection(config.db_stage);
//       const [results,] = await connection.execute(sql, params);
//       connection.end();
//       return results;
//     }
//     req.db_qa = async (sql, params) => {
//       const connection = await mysql.createConnection(config.db_stage);
//       const [results,] = await connection.execute(sql, params);
//       connection.end();
//       return results;
//     }
//     req.db_demo = async (sql, params) => {
//       const connection = await mysql.createConnection(config.db_demo);
//       const [results,] = await connection.execute(sql, params);
//       connection.end();
//       return results;
//     }
//     next();
//   }
// }

module.exports = {
  db
}