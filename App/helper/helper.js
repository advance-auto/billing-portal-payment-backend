'use strict';

const { OK } = require('http-status-codes');
const HttpStatus = require('http-status-codes');

// Success response helper


var success = (message, results, code) => {

    var pattern = {
        status: true,
        code: (code) ? code : HttpStatus.OK,
        message: message,
        data: results
    };

    return pattern;
};


//   Failure response helper


var failure = (e, code, flag) => {

    let message = "";
    if (e.message) {
        message = e.message;
    }
    if (!message) {
        message = e;
    }

    let data_ = null;

    try {
        if (flag)
            data_ = JSON.parse(message)
    } catch (error) { }


    var pattern = {
        status: false,
        code: (code) ? code : HttpStatus.BAD_REQUEST,
        message: message,
        data: data_
    };

    return pattern;

};

var formatDate = (format = 0, date = new Date()) => {
    let formattedDate;
    date = new Date(date)
    switch (format) {
        case 1:
            let day = date.getDate();
            let month = date.getMonth();
            let year = date.getFullYear();
            let time = date.toLocaleTimeString().split(' ')[0];
            formattedDate = `${year}-${month}-${day} ${time}`;;
            break;
        default:
            formattedDate = date.toDateString();
            break;
    }

    return formattedDate;
}


var valueCheck = (data, ErrorMessage) => {
    if (data)
        return data
    else
        throw new Error(ErrorMessage);
};

module.exports = {
    success: success,
    failure: failure,
    valueCheck: valueCheck,
    formatDate: formatDate
}