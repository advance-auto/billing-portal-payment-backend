'use strict';

const HttpStatus = require('http-status-codes');
const failure = require('../helper/helper').failure;
const success = require('../helper/helper').success;

const test = async (req, res, cb) => {
    try {
        let orgin = await req.get('origin');
        res.status(HttpStatus.OK).send(success("Response", orgin));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error))
    }
}


module.exports = {
    test: test
};
