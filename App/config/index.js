module.exports = function () {

  var config = {};
  switch (process.env.NODE_ENV) {

    case 'development':
      config = require('./env/development.json');
      break;

    case 'qa':
      config = require('./env/qa.json');
      break;

    case 'stage':
      config = require('./env/staging.json');
      break;

    case 'local':
      config = require('./env/local.json');
      break;
    
    case 'demo':
      config = require('./env/demo.json');
      break;

    default:
      console.error('NODE_ENV environment variable not set');
      process.exit(1);
  }

  return config;
};