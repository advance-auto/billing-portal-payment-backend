'use strict';

const HttpStatus = require('http-status-codes');
let cybersourceRestApi = require('cybersource-rest-client');
const { formatDate, success, failure, valueCheck } = require('../../helper/helper');

const capturePayments = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await capturePayment(req);
        let status = await updateStatus(resultObj, req);
        res.status(HttpStatus.OK).send(success("data", { status }));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}



const validate = async (req) => {
    if (!req.body.payment_id) throw new Error("Payment ID is Required!");
    let data = JSON.parse(JSON.stringify(await req.db(`SELECT p.totalAmount, p.currency, p.order_id, o.user_email FROM payment as p INNER JOIN orders as o ON p.order_id = o.id WHERE payment_id = ?`, [req.body.payment_id])))[0];
    if (!data.order_id) throw new Error("Invalid Payment ID");
    req.body.order_id = data.order_id;
    req.body.totalAmount = data.totalAmount;
    req.body.currency = data.currency;
    req.body.user_email = data.user_email;
}

const capturePayment = async (req) => {

    let tokenResponse = req.body.flexresponse

    let payment = req.app.get('config').payment;

    let reqBody = req.body;

    const configObj = payment.configObj;

    let apiClient = new cybersourceRestApi.ApiClient();
    let requestObj = new cybersourceRestApi.CapturePaymentRequest();

    let clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsClientReferenceInformation();
    clientReferenceInformation.code = req.body.user_email; // User ID
    requestObj.clientReferenceInformation = clientReferenceInformation;

    let orderInformation = new cybersourceRestApi.Ptsv2paymentsidcapturesOrderInformation();
    let orderInformationAmountDetails = new cybersourceRestApi.Ptsv2paymentsidcapturesOrderInformationAmountDetails();
    orderInformationAmountDetails.totalAmount = req.body.totalAmount; // Amount
    orderInformationAmountDetails.currency = req.body.currency; // Currency
    orderInformation.amountDetails = orderInformationAmountDetails;
    requestObj.orderInformation = orderInformation;

    let instance = new cybersourceRestApi.CaptureApi(configObj, apiClient);

    return await new Promise((err, resolve) => {
        instance.capturePayment(requestObj, req.body.payment_id, function (error, data, response) {
            if (error) err(error)
            else if (data) resolve(data);
        })
    }).then(
        err => { return { error: true, data: err } },
        responseData => { return { error: false, data: responseData } }
    );
}

const updateStatus = async ({ data, error }, req) => {
    
    let order_id = req.body.order_id;
    let payment_id = req.body.payment_id;
    let capture_payment_id = data.id;
    let totalAmount = data.orderInformation.amountDetails.totalAmount;
    let currency = data.orderInformation.amountDetails.currency;
    let status = data.status;
    let response = JSON.stringify(data);
    
    req.db(`INSERT INTO capture_payments SET order_id = ?, payment_id = ?, capture_payment_id = ?, totalAmount = ?, currency = ?, status = ?, response = ?`, [order_id, payment_id, capture_payment_id, totalAmount, currency, status, response])
    
    if (error) throw new Error(data.response.text);
    
    return status;
}

module.exports = {
    capturePayments
};
