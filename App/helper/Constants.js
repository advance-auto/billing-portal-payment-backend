'use strict';

module.exports = {
    
    FORBIDDEN_ERROR: "Sorry! You dont have the permission to access the resource!",
    INVALID_TOKEN : "Sorry! You have an invalid authentication token!",
    EXPIRED_TOKEN : "Sorry! Your token has expired!"
}