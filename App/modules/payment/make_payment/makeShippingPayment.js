'use strict';

const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
let cybersourceRestApi = require('cybersource-rest-client');
const { formatDate, success, failure, valueCheck } = require('../../../helper/helper');
const sendMail = require('../../../helper/orderMail').sendMail;

const getReceipt = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await generateReceipt(req);
        let receiptData = await savePaymentHistory(resultObj, req);
        res.status(HttpStatus.OK).send(success("Receipt", receiptData));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}



const validate = async (req) => {
    // if (!req.body.flexresponse && (!req.body.customer && !req.body.code)) throw new Error("FLEXRESPONSE or (Customer ID and Code) is required");
    if (!req.body.flexresponse) throw new Error("FLEXRESPONSE is required");
    if (!req.body.unique_id) throw new Error("Unique ID is required");
    let data = (await req.db(`SELECT id, shipping_amount as amount, seller_address_id as billing_address_id FROM order_products WHERE shipping_unique_id = ?`, [req.body.unique_id]))[0];

    if (data.length == 0) throw new Error("Invalid Unique ID");
    let { id, amount, billing_address_id: billing_address } = data;
    req.body.amount = amount;
    req.body.shipping_address = billing_address;

}

const generateReceipt = async (req) => {

    let tokenResponse = req.body.flexresponse

    let payment = req.app.get('config').payment;

    let reqBody = req.body;

    const configObj = payment.configObj;

    let instance = new cybersourceRestApi.PaymentsApi(configObj);

    let clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsClientReferenceInformation();

    clientReferenceInformation.code = req.authUser.email; // reqBody.clientReferenceInformationcode; // clientReferenceInformation.code should be parsed from the token barer from the request object 

    var processingInformation = new cybersourceRestApi.Ptsv2paymentsProcessingInformation();
    processingInformation.commerceIndicator = "internet"; //reqBody.commerceIndicator;

    var amountDetails = new cybersourceRestApi.Ptsv2paymentsOrderInformationAmountDetails();
    amountDetails.totalAmount = reqBody.amount;
    amountDetails.currency = req.app.get('config').currency;

    let query = 'SELECT country, first_name,last_name,phone,address1,postcode,city as locality,state as administrativeArea,email,address2,state as district FROM `addresses` WHERE id = ?';
    let result = await req.db(query, [req.body.shipping_address])
    if (result.length == 0) throw new Error('Invalid Address ID');
    result = result[0]

    let billTo = new cybersourceRestApi.Ptsv2paymentsOrderInformationBillTo();
    billTo.country = valueCheck(result.country, "Country cannot be empty!");
    billTo.firstName = valueCheck(result.first_name, "first_name cannot be empty!");
    billTo.lastName = valueCheck(result.last_name, "last_name cannot be empty!") //? result.last_name : "Las";
    billTo.phoneNumber = valueCheck(result.phone, "phone cannot be empty!");
    billTo.address1 = valueCheck(result.address1, "address1 cannot be empty!");
    billTo.postalCode = valueCheck(result.postcode, "postcode cannot be empty!");
    billTo.locality = valueCheck(result.locality, "locality cannot be empty!"); // ?
    billTo.administrativeArea = valueCheck(result.administrativeArea, "administrativeArea cannot be empty!"); // ?
    billTo.email = valueCheck(result.email, "email cannot be empty!");
    billTo.address2 = valueCheck(result.address2, "address2 cannot be empty!");


    let orderInformation = new cybersourceRestApi.Ptsv2paymentsOrderInformation();
    orderInformation.amountDetails = amountDetails;
    orderInformation.billTo = billTo;

    if (req.body.save && req.body.save === 'true') {
        processingInformation.actionList = ["TOKEN_CREATE"];
        processingInformation.actionTokenTypes = ["customer", "paymentInstrument", "shippingAddress"];
    }

    var tokenInformation = new cybersourceRestApi.Ptsv2paymentsTokenInformation();
    tokenInformation.transientTokenJwt = tokenResponse ? tokenResponse : req.body.code;

    let paymentInformation = new cybersourceRestApi.Ptsv2paymentsPaymentInformation();

    if (req.body.customer && req.body.code) {

        let code = req.body.code;
        let profile = await req.db('SELECT `id`, `card_name`, `instrument_identifier`, `payment_instrument` FROM `cards` WHERE user_id = ? AND id = ?', [req.authUser.id, req.body.customer]);
        if (!profile.length) throw new Error("Customer ID is not found for this user.")
        let card_type = profile[0].card_name;

        paymentInformation.instrumentIdentifier = { id: profile[0].instrument_identifier };
        paymentInformation.card = await getPaymentInstrument(cybersourceRestApi, profile[0].payment_instrument, card_type, req);
    }


    var request = new cybersourceRestApi.CreatePaymentRequest();

    request.clientReferenceInformation = clientReferenceInformation;
    request.processingInformation = processingInformation;
    request.orderInformation = orderInformation;
    request.tokenInformation = tokenInformation;
    if (req.body.customer) request.paymentInformation = paymentInformation;
    console.log(request);

    let resPro = new Promise((err, resolve) => {
        instance.createPayment(request, function (error, data, response) {

            if (error) err(error);
            else if (data) resolve(data);
        });
    })

    return await resPro.then(
        err => { return { error: true, data: err } },
        responseData => { return { error: false, data: responseData } }
    );
}

const savePaymentHistory = async (response, req) => {

    if (response.error)
        throw new Error(response.data.response.text);

    // Save a payment profile
    if ((req.body.save && req.body.save === 'true') && response.data.tokenInformation) {

        var decoded = await jwt.decode(req.body.flexresponse);
        const profile_card_number = (decoded && decoded.data && decoded.data.number) ? decoded.data.number : false;
        if (!profile_card_number) throw new Error('invalid token');

        let profile = await req.db('SELECT * FROM `cards` WHERE user_id = ?', [req.authUser.id]);
        const firstTime = profile[0] ? false : true;
        const cardName = response.data.paymentInformation.card.type; //001
        const lastFourDigit = profile_card_number.slice(profile_card_number.length - 4);
        const date = formatDate(1);
        const { paymentInstrument: { id: paymentInstrument }, shippingAddress: { id: shippingAddress }, instrumentIdentifier: { id: instrumentIdentifier } } = response.data.tokenInformation;

        profile = JSON.parse(JSON.stringify(profile));

        let isCardAdded = profile.reduce((isAlreadyAdded, card) => {
            if (card.instrument_identifier === instrumentIdentifier) return true;
            if (isAlreadyAdded) return true;
        }, false);

        if (!isCardAdded) {

            let query = 'INSERT INTO `cards`(`user_id`, `card_name`, `instrument_identifier`, `payment_instrument`, `shipping_address`, `is_default`, `created_at`) VALUES (?,?,?,?,?,?,?);';
            let values = [req.authUser.id, cardName, instrumentIdentifier, paymentInstrument, shippingAddress, firstTime, date];

            req.db(query, values);
        }
    }

    let status;
    if (response.data.payment_info && response.data.payment_info.status) status = response.data.payment_info.status;
    else if (response.data.status) status = response.data.status;
    else status = "unknown";

    let responceData = JSON.stringify(response.data)
    let query = 'INSERT INTO `payment`(`status`, `response`, `order_id`) VALUES (?,?,?)';

    let result = await req.db(query, [((status == "AUTHORIZED") ? "successful" : "failure"), responceData, req.body.order_id ? req.body.order_id : null]);

    if (status == "AUTHORIZED") {
        query = `UPDATE order_products SET shipping_paid = 1 WHERE unique_id = ?`;
        await req.db(query, [req.body.unique_id]);
    }

    return response.data;
}

module.exports = {
    getReceipt
};