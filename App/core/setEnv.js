'use strict';

const dev = require('../config/env/development.json');
const local = require('../config/env/local.json');
const qa = require('../config/env/qa.json');
const stage = require('../config/env/staging.json');
const demo = require('../config/env/demo.json');

const env = async (req, res, next) => {

    // const ORGINS = req.app.get('config').env;
    const origin = req.get('origin');
    console.log(origin);

    // let name = await ORGINS.reduce((from, orgin) => {
    //     if (orgin.url == origin) {
    //         return orgin.name;
    //     }
    // }, null)

    // console.log(req.get('origin'), name);

    switch (origin) {
        case "http://localhost:8080":
            req.app.set("config", local);
            req.db = req.db_dev;
            console.log("local");
            break;
        case "https://dev.liiighthouse.net":
            req.app.set("config", dev);
            req.db = req.db_dev;
            console.log("dev");
            break;
        case "https://qa-frontend.liiighthouse.net":
            req.app.set("config", qa)
            req.db = req.db_qa;
            console.log("qa");
            break;
        case "https://stage.liiighthouse.net":
            req.app.set("config", stage)
            req.db = req.db_stage;
            console.log("stage");
            break;
        case "https://pay-demo.liiighthouse.net":
            req.app.set("config", demo)
            req.db = req.db_demo;
            console.log("stage");
            break;
        default:
            console.log("unknown orgin");
            break;
    }

    next();
}

module.exports = {
    setEnv: env
};