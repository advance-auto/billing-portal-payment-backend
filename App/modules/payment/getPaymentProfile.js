'use strict';

const HttpStatus = require('http-status-codes');
const failure = require('../../helper/helper').failure;
const success = require('../../helper/helper').success;
let cybersourceRestApi = require('cybersource-rest-client');

const getPaymentProfile = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let result = await getProfile(req);
        res.status(HttpStatus.OK).send(success("Payment token listed successfully!", result ? result : []));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}

const validate = async (req) => { }

const getProfile = async (req) => {

    let query = 'SELECT `id`, `instrument_identifier`, `card_name`, `is_default` FROM `cards` WHERE user_id = ?';
    let cards = await req.db(query, [req.authUser.id]);

    if (cards.length !== 0) {
        cards = JSON.parse(JSON.stringify(cards));
        return await retrieveInstrumentIdentifier(req, cards)
    }
}

const retrieveInstrumentIdentifier = async (req, cards) => {

    let payment = req.app.get('config').payment;
    const configObj = payment.configObj;

    let apiClient = new cybersourceRestApi.ApiClient();

    let instance = new cybersourceRestApi.InstrumentIdentifierApi(configObj, apiClient);

    for (const card of cards) {

        let id = card.instrument_identifier;

        let cardPromise = new Promise((data, err) => {
            instance.getInstrumentIdentifier(id, [], function (error, data_, response) {
                if (error) err(error)
                else if (data_) data(data_)
            });
        })

        let cardDetail = await cardPromise.then(data => data, err => { throw new Error(err) });
        card.last_four = cardDetail.card.number;
        delete card.instrument_identifier;

    }

    return cards;
}

module.exports = {
    getPaymentProfile: getPaymentProfile
};