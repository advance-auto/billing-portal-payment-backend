'use strict';

const HttpStatus = require('http-status-codes');
let cybersourceRestApi = require('cybersource-rest-client');
const { formatDate, success, failure, valueCheck } = require('../../helper/helper');

const authorizationReversal = async (req, res, cb) => {
    try {
        let isValid = await validate(req);
        let resultObj = await reversal(req);
        let status = await updateStatus(resultObj, req);
        res.status(HttpStatus.OK).send(success("data", { status }));
    } catch (error) {
        res.status(HttpStatus.BAD_REQUEST).send(failure(error, null, true))
    }
}



const validate = async (req) => {
    if (!req.body.payment_id) throw new Error("Payment ID is Required!");
    let data = JSON.parse(JSON.stringify(await req.db(`SELECT p.totalAmount, p.currency, p.order_id, o.user_email FROM payment as p INNER JOIN orders as o ON p.order_id = o.id WHERE payment_id = ?`, [req.body.payment_id])))[0];
    if (!data.order_id) throw new Error("Invalid Payment ID");
    req.body.order_id = data.order_id;
    req.body.totalAmount = data.totalAmount;
    req.body.user_email = data.user_email;
}

const reversal = async (req) => {

    let payment = req.app.get('config').payment;

    const configObj = payment.configObj;


    var apiClient = new cybersourceRestApi.ApiClient();
    var requestObj = new cybersourceRestApi.AuthReversalRequest();

    var clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsidreversalsClientReferenceInformation();
    clientReferenceInformation.code = req.body.user_email; // User ID
    requestObj.clientReferenceInformation = clientReferenceInformation;

    var reversalInformation = new cybersourceRestApi.Ptsv2paymentsidreversalsReversalInformation();
    var reversalInformationAmountDetails = new cybersourceRestApi.Ptsv2paymentsidreversalsReversalInformationAmountDetails();
    reversalInformationAmountDetails.totalAmount = req.body.totalAmount;
    reversalInformation.amountDetails = reversalInformationAmountDetails;

    reversalInformation.reason = req.body.reason;
    requestObj.reversalInformation = reversalInformation;


    var instance = new cybersourceRestApi.ReversalApi(configObj, apiClient);

    return await new Promise((err, resolve) => {
        instance.authReversal(req.body.payment_id, requestObj, function (error, data, response) {
            if (error) err(error)
            else if (data) resolve(data);
        });
    }).then(
        err => { return { error: true, data: err } },
        responseData => { return { error: false, data: responseData } }
    );

}

const updateStatus = async ({ data, error }, req) => {

    let response = JSON.stringify(data);
    let payment_id = req.body.payment_id;
    let order_id = req.body.order_id
    let status = data.status;
    let totalAmount = req.body.totalAmount;
    let reversal_id = data.id ? data.id : '0';

    // req.db(`UPDATE payment SET reveral_response = ?, authorization_status = ? WHERE payment_id = ?`, [response, status, req.body.payment_id]);
    req.db(`INSERT INTO payment_cancel_reversal (response, payment_id, order_id, status, totalAmount, reversal_id) VALUES (?,?,?,?,?,?)`, [response, payment_id, order_id, status, totalAmount, reversal_id]);
    // req.db(`INSERT INTO payment_cancel_reversal SET response = ?, payment_id = ?, order_id = ?, status = ?, totalAmount = ?, reversal_id = ? WHERE payment_id = '${req.body.payment_id}'`, [response, payment_id, order_id, status, totalAmount, reversal_id]);

    // if (error) throw new Error(data.response.text);

    return status;
}

module.exports = {
    authorizationReversal
};
