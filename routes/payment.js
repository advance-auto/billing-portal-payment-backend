let Env = require('../App/core/setEnv');
let Security = require('../App/core/authentication');
let ServerAuth = require('../App/core/serverAuthentication');
let paymentReceipt = require('../App/modules/payment/make_payment');
let paymentSubscription = require('../App/modules/payment/make_paryment_subscription');
let generatePublicKey = require('../App/modules/payment/generatePublicKey');
let savePaymentProfile = require('../App/modules/payment/savePaymentProfile');
let PaymentProfile = require('../App/modules/payment/getPaymentProfile');
let SupportLiiighthousePayment = require('../App/modules/payment/make_payment/makeSupportLiiighthousePayment');
let MakeLight = require('../App/modules/payment/make_payment/makeLightPayment');
let CapturePayments = require('../App/modules/payment/capturePayment');
let CancelPayment = require('../App/modules/payment/authorizationReversal');
let Refund = require('../App/modules/payment/capturesRefunds');
let Seller = require('../App/modules/payment/make_payment/sellerPayment');
let Shipping = require('../App/modules/payment/make_payment/makeShippingPayment');

module.exports = (app) => {
    app.post("/make/payment/local", paymentSubscription.getReceipt);
    app.post("/generate/publickey/local", generatePublicKey.generatePublicKey);
    app.post("/make/payment", paymentReceipt.getReceipt);
    app.post("/generate/publickey", generatePublicKey.generatePublicKey);
    app.post("/save/profile", Security.authorize, savePaymentProfile.savePayment);
    app.get("/get/profiles", Security.authorize, PaymentProfile.getPaymentProfile);
    app.post("/make/support/payment", Security.authorize, SupportLiiighthousePayment.getReceipt);
    app.post("/make/liiight/payment", Security.authorize, MakeLight.getReceipt);
    app.post("/make/shipping/payment", Security.authorize, Shipping.getReceipt);
    app.post("/capture/payment", ServerAuth.EAUTH, CapturePayments.capturePayments);
    app.post("/cancel/payment", ServerAuth.EAUTH, CancelPayment.authorizationReversal);
    app.post("/refund/payment", ServerAuth.EAUTH, Refund.capturesRefunds);
    app.post("/seller/payment", Security.authorize, Seller.getReceipt);
    app.post("/test", (req, res) => {
        if (!req.body.test) res.status(400).json({ cs_status: "Bad Request" });
        else res.status(200).json({ cs_status: "OK" });
    });
};